## Optional: Making the working environment less cluttered

In VSCode, press <keyboard>ctrl</keyboard><keyboard>shift</keyboard><keyboard>p</keyboard>, and write "open workspace settings"

We're going to hide the files we don't want to see, so they don't clutter our tree. Look for `files.exclude`, and add:

- `**/*node_modules` to hide that folder
- `**/*.vscode` to hide the settings themselves
- `**/*package-lock.json`

While you're at it, set the tab size to `2` to enforce editor-level consistency when handling indents